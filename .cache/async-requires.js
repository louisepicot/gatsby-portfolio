// prefer default export if available
const preferDefault = m => (m && m.default) || m

exports.components = {
  "component---cache-dev-404-page-js": () => import("./../../dev-404-page.js" /* webpackChunkName: "component---cache-dev-404-page-js" */),
  "component---src-pages-404-js": () => import("./../../../src/pages/404.js" /* webpackChunkName: "component---src-pages-404-js" */),
  "component---src-pages-index-js": () => import("./../../../src/pages/index.js" /* webpackChunkName: "component---src-pages-index-js" */),
  "component---src-pages-infos-js": () => import("./../../../src/pages/infos.js" /* webpackChunkName: "component---src-pages-infos-js" */),
  "component---src-pages-order-js": () => import("./../../../src/pages/order.js" /* webpackChunkName: "component---src-pages-order-js" */),
  "component---src-pages-pizza-js": () => import("./../../../src/pages/pizza.js" /* webpackChunkName: "component---src-pages-pizza-js" */),
  "component---src-pages-projects-list-js": () => import("./../../../src/pages/projectsList.js" /* webpackChunkName: "component---src-pages-projects-list-js" */),
  "component---src-pages-slicemasters-js": () => import("./../../../src/pages/slicemasters.js" /* webpackChunkName: "component---src-pages-slicemasters-js" */)
}

