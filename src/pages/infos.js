import React from 'react';
import GridDiv from '../components/GridDiv';

const InfosPage = () => (
  <GridDiv>
    <div>
      <ul id="titles">
        <li />
        <li />
        <li />
      </ul>
    </div>

    <div>
      <h2>text</h2>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum facere
        nulla necessitatibus veritatis mollitia magnam suscipit quasi, autem
        expedita ullam recusandae sit impedit qui amet quae tenetur nesciunt
        quas ex.
      </p>
    </div>
    <div>
      <img src="" alt="" />
      <img src="" alt="" />
      <img src="" alt="" />
      <img src="" alt="" />
      <img src="" alt="" />
      <img src="" alt="" />
    </div>
  </GridDiv>
);

export default InfosPage;
