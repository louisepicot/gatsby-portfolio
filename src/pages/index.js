import React from 'react';
import { graphql } from 'gatsby';
import ProjectsList from './projectsList';

const HomePage = ({ data }) => {
  console.log(data.projects.nodes);
  const projects = data.projects.nodes;
  return (
    <>
      <ProjectsList projects={projects} />
    </>
  );
};
export default HomePage;

export const query = graphql`
  query ProjectQuery {
    projects: allSanityProject {
      nodes {
        date
        name
        description
        slug {
          current
        }
        tags {
          name
        }
        imagesGallery {
          asset {
            fluid(maxWidth: 400) {
              ...GatsbySanityImageFluid
            }
          }
        }
      }
    }
  }
`;
