import React from 'react';
import GridDiv from '../components/GridDiv';

const ProjectsList = ({ projects }) => (
  <GridDiv>
    <div>
      <ul id="titles">
        {projects.map((project) => (
          <div key={project.name}>
            <p>{project.name}</p>
            <ul>
              {project.tags.map((tag) => (
                <li key={tag.name}>{tag.name}</li>
              ))}
            </ul>
            {project.imagesGallery.map((image) => (
              <img src={image.asset.fluid.src} alt="" />
            ))}
          </div>
        ))}
      </ul>
    </div>

    <div>
      <h2>text</h2>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum facere
        nulla necessitatibus veritatis mollitia magnam suscipit quasi, autem
        expedita ullam recusandae sit impedit qui amet quae tenetur nesciunt
        quas ex.
      </p>
    </div>
    <div>
      <img src="" alt="" />
      <img src="" alt="" />
      <img src="" alt="" />
      <img src="" alt="" />
      <img src="" alt="" />
      <img src="" alt="" />
    </div>
  </GridDiv>
);

export default ProjectsList;
