import React from 'react';
import styled from 'styled-components';
import 'normalize.css';

const Grid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr auto;
  background: red;
`;

const GridDiv = ({ children }) => <Grid>{children}</Grid>;

export default GridDiv;
