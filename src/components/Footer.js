import React from 'react';

const Footer = () => (
  <footer>
    <p> Footer {new Date().getFullYear()}</p>
  </footer>
);

export default Footer;
