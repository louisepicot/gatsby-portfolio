import React from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';

const NavStyles = styled.nav`
  width: 100%;
  ul {
    width: 100%;
    display: flex;
    justify-content: space-around;
  }
`;

const Nav = () => (
  <NavStyles>
    <h1>Louise Picot</h1>
    <ul>
      <li>
        <Link to="/">home</Link>
      </li>
      <li>
        <Link to="/pizza">pizza</Link>
      </li>
      <li>
        <Link to="/infos">infos</Link>
      </li>
      <li>
        <Link to="/order">oreder</Link>
      </li>
      <li>
        <Link to="/slicemasters">slicemasters</Link>
      </li>
    </ul>
  </NavStyles>
);

export default Nav;
