import { createGlobalStyle } from 'styled-components';

import font from '../assets/fonts/ISOBARE/Isobare-Regular.woff';
import quebrado from '../assets/fonts/Quebrado-Regular.otf';

const Typography = createGlobalStyle`
  @font-face {
    font-family: Isobare;
    src: url(${font});
  }

  @font-face {
    font-family: Quebrado;
    src: url(${quebrado});
  }
  html {
    font-family: Quebrado, Times New Roman, serif;;
    color: var(--black);
  }
  p, li {
    letter-spacing: 0.5px;
  }
  h1,h2,h3,h4,h5,h6 {
    font-family: Isobare, Times New Roman, serif;;
    font-size: 4em;
    font-weight: normal;
    margin: 0;
  }
  a {
    color: var(--black);
    text-decoration-color: var(--black);
  }
  mark, .mark {
    background: var(--yellow);
    padding: 0 2px 2px 2px;
    margin: 0;
    display: inline;
    line-height: 1;
  }

  .center {
    text-align: center;
  }

  .tilt {
    transform: rotate(-2deg);
  }
`;

export default Typography;
