import { createGlobalStyle } from 'styled-components';

const GlobalStyles = createGlobalStyle`
  :root {
    --black: #000000;
    --green: #00FF3C;
    --white: #fff;
    --grey: #f4f4f4;
  }

  *, *:before, *:after, html {
	margin: 0;
	padding: 0;
	
	-webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
	box-sizing: border-box;
}

  html {
    font-size: 10px;
  }

  body {
	/* font-family: Times New Roman, serif; */

	background-color: var(--grey);
}

h1, h2, p, a {
	font-size: 16px;
	font-weight: 400;
	color: black;
}

a { text-decoration: underline; }
a:hover { text-decoration: none; }


nav {
	width: 100vw;
	padding: 0 20px;
}


ul#texts > li#shamanism { border-top: 0; }
ul#texts > li#body_farming { margin-top: 26px; }
ul#texts > li#kidney { margin-top: 53px; }
ul#texts > li#asteroid { margin-top: 80px; }
ul#texts > li#control { margin-top: 107px; }

ul#texts > li > p {	margin-bottom: 12px; }
ul#texts > li > p:last-child { margin-bottom: 0; }







  .gatsby-image-wrapper img[src*=base64\\,] {
    image-rendering: -moz-crisp-edges;
    image-rendering: pixelated;
  }

  /* Scrollbar Styles */
  body::-webkit-scrollbar {
    width: 12px;
  }
  html {
    scrollbar-width: thin;
    scrollbar-color: var(--red) var(--white);
  }
  body::-webkit-scrollbar-track {
    background: var(--white);
  }
  body::-webkit-scrollbar-thumb {
    background-color: var(--red) ;
    border-radius: 6px;
    border: 3px solid var(--white);
  }

  img {
    max-width: 100%;
  }

/* Moblile */
@media screen and ( min-width: 0px ) and ( max-width: 1057px ) {

nav { margin: 10px 0; }

ul#titles { width: 100vw; }
ul#texts { width: 100vw; }

#wrap > ul {
    display: inline-block;
    float: left;
    overflow-y: scroll;
}

ul#texts > li#shamanism,
ul#texts > li#kidney,
ul#texts > li#body_farming,
ul#texts > li#asteroid,
ul#texts > li#control { margin-top: 0; }

ul#titles > li {
    line-height: 25px;
    padding-left: 22px;

    border-top: 1px solid black;

    cursor: pointer;
}
ul#titles > li:last-child {	border-bottom: 1px solid black;	}

ul#texts > li {
    padding-top: 0;
    border: 0;
}

ul#imagesMobile { width: 100vw; }

ul#imagesMobile > ul {
    padding: 20px calc(22px - 2.5%);
    display: none;
}

ul#imagesMobile > ul > img.hidden,
ul#imagesMobile > ul > video.hidden { display: inline-block; }

img, video {
    width: 20%;
    margin: 0 2.5% 5% 2.5%;
    padding: 0;
    border: 1px solid black;
    float: left;
}

ul#imagesDesktop { display: none; }

}

/* Everything else */
@media screen and ( min-width: 1058px ) {

nav {
    line-height: 30px;
    border-bottom: 1px solid black;
}

ul#titles { width: 330px; }
ul#texts { width: 400px; }

#wrap > ul {
    height: calc( 100vh - 31px );

    display: inline-block;
    float: left;
    overflow-y: scroll;
}

ul#titles > li {
    line-height: 25px;
    padding-top: 1px;
    padding-left: 22px;

    border-bottom: 1px solid black;

    cursor: pointer;
}

ul#texts {
    border-left: 1px solid black;
    border-right: 1px solid black;
}


ul#imagesMobile { display: none; }
ul#imagesDesktop {}

ul#imagesDesktop > ul {
    padding: 5% 22px;

    border-top: 1px solid black;

    display: none;
}

ul#imagesDesktop > ul > img.hidden,
ul#imagesDesktop > ul > video.hidden { display: inline-block; }

img, video {
    width: 20%;
    margin: 0 2.5% 5% 2.5%;
    padding: 0;

    border: 1px solid black;

    cursor: pointer;
    float: left;
}

/* ul#imagesDesktop > ul.shamanism { border-top: 0; }
ul#imagesDesktop > ul.body_farming { margin-top: 26px; }
ul#imagesDesktop > ul.kidney { margin-top: 53px; }
ul#imagesDesktop > ul.asteroid { margin-top: 80px; }
ul#imagesDesktop > ul.control { margin-top: 107px; } */

}

`;

export default GlobalStyles;
